from Initialization import chrome_setup  # , firefox_setup, edge_setup, chrome_incognito
from Urls import Urls
from Methods import Methods
from Locators import Compressor
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class CompressorSizeExceeded(Urls, Methods, Compressor):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Compressor.environment_definition(environment))
        action.wait(Compressor.anchor)
        action.clicker(Compressor.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\PDFs\~7.5.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(Compressor.best_quality)
        sleep(2)
        action.clicker(Compressor.low_quality)
        sleep(1)
        action.clicker(Compressor.compress_files)
        action.wait(Compressor.anchor_2)
        sleep(1)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class CompressorDailyLimit(Urls, Methods, Compressor):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Compressor.environment_definition(environment))

        action.wait(Compressor.anchor)
        action.clicker(Compressor.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(Compressor.best_quality)
        sleep(2)
        action.clicker(Compressor.low_quality)
        sleep(1)
        action.clicker(Compressor.compress_files)
        action.wait(Compressor.hide)
        sleep(3)
        action.clicker(Compressor.hide)
        sleep(2)
        action.clicker(Compressor.download_btn)
        sleep(7)
        action.go_back()
        action.wait(Compressor.anchor)
        sleep(1)

        action.clicker(Compressor.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(Compressor.best_quality)
        sleep(2)
        action.clicker(Compressor.moderate_quality)
        sleep(1)
        action.clicker(Compressor.compress_files)
        action.wait(Compressor.hide)
        sleep(3)
        action.clicker(Compressor.hide)
        sleep(2)
        action.clicker(Compressor.download_btn)
        sleep(7)
        action.go_back()
        action.wait(Compressor.anchor)
        sleep(1)

        action.clicker(Compressor.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(Compressor.best_quality)
        sleep(2)
        action.clicker(Compressor.best_quality)
        sleep(1)
        action.clicker(Compressor.compress_files)
        action.wait(Compressor.hide)
        sleep(3)
        action.clicker(Compressor.hide)
        sleep(2)
        action.clicker(Compressor.download_btn)
        sleep(7)
        action.go_back()
        action.wait(Compressor.anchor)
        sleep(1)

        action.clicker(Compressor.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(Compressor.best_quality)
        sleep(2)
        action.clicker(Compressor.low_quality)
        sleep(1)
        action.clicker(Compressor.compress_files)
        action.wait(Compressor.anchor_2)
        sleep(1)

        try:
            assert "You've reached your limit of 3 tasks per hour" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = CompressorDailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = CompressorSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
