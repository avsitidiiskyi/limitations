from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import PDF2HTML
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class PDF2HTMLSizeExceeded(Urls, Methods, PDF2HTML):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Pdf2Html.environment_definition(environment))
        loader.press('f12')
        action.wait(PDF2HTML.anchor)
        action.clicker(PDF2HTML.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\MINI.pdf")
        loader.press('enter')
        sleep(5)
        action.wait(PDF2HTML.dwnlwd)
        sleep(1)
        action.clicker(PDF2HTML.dwnlwd)
        sleep(1)
        action.wait(PDF2HTML.to_wait)
        sleep(2)

        try:
            assert "free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class PDF2HTMLDailyLimit(Urls, Methods, PDF2HTML):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Pdf2Html.environment_definition(environment))
        loader.press('f12')

        action.wait(PDF2HTML.anchor)
        action.clicker(PDF2HTML.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(2)
        loader.press('enter')
        action.wait(PDF2HTML.dwnlwd)
        sleep(3)
        action.clicker(PDF2HTML.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(PDF2HTML.anchor)
        action.clicker(PDF2HTML.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(2)
        loader.press('enter')
        action.wait(PDF2HTML.dwnlwd)
        sleep(3)
        action.clicker(PDF2HTML.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(PDF2HTML.anchor)
        action.clicker(PDF2HTML.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        sleep(2)
        loader.press('enter')
        action.wait(PDF2HTML.dwnlwd)
        sleep(3)
        action.clicker(PDF2HTML.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(PDF2HTML.anchor)
        action.clicker(PDF2HTML.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\small 1.pdf")
        loader.press('enter')
        sleep(2)
        action.wait(PDF2HTML.to_wait)
        sleep(1)

        try:
            assert "enjoyed using our free service" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = PDF2HTMLDailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = PDF2HTMLSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)




