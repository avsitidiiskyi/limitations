from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import GifToPDF
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class GIFToPdfSizeExceeded(Urls, Methods, GifToPDF):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Gif2Pdf.environment_definition(environment))
        action.wait(GifToPDF.anchor)
        action.clicker(GifToPDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Images\6 mb.jpg")
        loader.press('enter')
        sleep(5)

        action.wait(GifToPDF.text)
        sleep(1)

        try:
            assert "Oops! Looks like you've really enjoyed using our free service..." in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class GifToPdfDailyLimit(Urls, Methods, GifToPDF):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Gif2Pdf.environment_definition(environment))

        action.wait(GifToPDF.anchor)
        action.clicker(GifToPDF.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(GifToPDF.hide)
        sleep(3)
        action.clicker(GifToPDF.hide)
        sleep(2)
        action.clicker(GifToPDF.download_btn)
        sleep(7)
        action.go_back()
        action.wait(GifToPDF.anchor)
        sleep(1)

        action.wait(GifToPDF.anchor)
        action.clicker(GifToPDF.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(GifToPDF.hide)
        sleep(3)
        action.clicker(GifToPDF.hide)
        sleep(2)
        action.clicker(GifToPDF.download_btn)
        sleep(7)
        action.go_back()
        action.wait(GifToPDF.anchor)
        sleep(1)

        action.wait(GifToPDF.anchor)
        action.clicker(GifToPDF.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(GifToPDF.hide)
        sleep(3)
        action.clicker(GifToPDF.hide)
        sleep(2)
        action.clicker(GifToPDF.download_btn)
        sleep(7)
        action.go_back()
        action.wait(GifToPDF.anchor)
        sleep(1)

        action.clicker(GifToPDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        loader.press('enter')
        sleep(5)

        action.wait(GifToPDF.text)
        sleep(1)

        try:
            assert "Oops! Looks like you've really enjoyed using our free service..." in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = GifToPdfDailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = GIFToPdfSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
