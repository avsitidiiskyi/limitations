from Initialization import chrome_setup  # , firefox_setup, edge_setup, chrome_incognito
from Urls import Urls
from Methods import Methods
from Locators import PdfMerge
from time import sleep
import pyautogui as loader
from variable_storage import *
location = "tedsad"# location_definer()


class MergeSizeExceeded(Urls, Methods, PdfMerge):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.PdfMerge.environment_definition(environment))
        action.wait(PdfMerge.anchor)
        action.clicker(PdfMerge.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs")
        loader.press('enter')
        sleep(1)
        loader.typewrite('"~7.5.pdf" "small 1.pdf"')
        sleep(1)
        #loader.press('enter')
        while True:
            try:
                check = action.check_exists_by_xpath(PdfMerge.merge_file_btn)
                if check:
                    print('located')
                    break

            except Exception:
                print("nope")
        sleep(5)
        action.clicker(PdfMerge.merge_file_btn)
        action.wait(PdfMerge.anchor_2)
        sleep(1)



if __name__ == "__main__":

    init = MergeSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
