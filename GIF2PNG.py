from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import Gif2Png
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class SizeExceeded(Urls, Methods, Gif2Png):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.GIF2PNG.environment_definition(environment))
        action.wait(Gif2Png.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(Gif2Png.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\GIFs\over 3.gif")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Gif2Png.to_wait)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Gif2Png.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Gif2Png.to_wait)
        sleep(1)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class DailyLimit(Urls, Methods, Gif2Png):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.GIF2PNG.environment_definition(environment))

        action.wait(Gif2Png.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(Gif2Png.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\GIFs\~3.gif")
        sleep(1)
        loader.press('enter')
        while True:
            try:
                success = action.check_exists_by_xpath(Gif2Png.dwnlwd)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Gif2Png.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        action.wait(Gif2Png.dwnlwd)
        sleep(3)
        action.clicker(Gif2Png.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(Gif2Png.anchor)
        action.clicker(Gif2Png.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\GIFs\~3.gif")
        sleep(1)
        loader.press('enter')
        while True:
            try:
                success = action.check_exists_by_xpath(Gif2Png.dwnlwd)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Gif2Png.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        action.wait(Gif2Png.dwnlwd)
        sleep(3)
        action.clicker(Gif2Png.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(Gif2Png.anchor)
        action.clicker(Gif2Png.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\GIFs\~3.gif")
        sleep(1)
        loader.press('enter')
        while True:
            try:
                success = action.check_exists_by_xpath(Gif2Png.dwnlwd)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Gif2Png.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        action.wait(Gif2Png.dwnlwd)
        sleep(3)
        action.clicker(Gif2Png.dwnlwd)
        sleep(3)
        action.go_back()

        action.clicker(Gif2Png.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\GIFs\~3.gif")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Gif2Png.to_wait)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Gif2Png.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Gif2Png.to_wait)
        sleep(1)

        try:
            assert "Oops! Looks like you've really" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = DailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = SizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
