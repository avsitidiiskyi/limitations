from Initialization import chrome_setup  # , firefox_setup, edge_setup, chrome_incognito
from Urls import Urls
from Methods import Methods
from Locators import PDF2PPT
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class PDF2PPTSizeExceeded(Urls, Methods, PDF2PPT):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Pdf2ppt.environment_definition(environment))
        action.wait(PDF2PPT.anchor)
        sleep(1)
        action.clicker(PDF2PPT.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\MINI.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(PDF2PPT.to_wait)
        sleep(2)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class PDF2PPTDailyLimit(Urls, Methods, PDF2PPT):

    @staticmethod
    def four_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Pdf2ppt.environment_definition(environment))
        action.wait(PDF2PPT.anchor)
        sleep(1)
        action.clicker(PDF2PPT.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(PDF2PPT.download)
        sleep(3)
        action.clicker(PDF2PPT.download)
        sleep(5)
        action.go_back()

        action.wait(PDF2PPT.anchor)
        sleep(1)
        action.clicker(PDF2PPT.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(PDF2PPT.download)
        sleep(3)
        action.clicker(PDF2PPT.download)
        sleep(5)
        action.go_back()

        action.wait(PDF2PPT.anchor)
        sleep(1)
        action.clicker(PDF2PPT.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(PDF2PPT.download)
        sleep(3)
        action.clicker(PDF2PPT.download)
        sleep(5)
        action.go_back()

        action.wait(PDF2PPT.anchor)
        sleep(1)
        action.clicker(PDF2PPT.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        sleep(1)
        loader.press('enter')
        sleep(1)
        action.wait(PDF2PPT.to_wait)
        sleep(2)

        try:
            assert "enjoyed using our free service" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = PDF2PPTDailyLimit
    driver = chrome_setup()
    init.four_file_loader(driver)

    init = PDF2PPTSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
