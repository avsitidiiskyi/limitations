from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import DocX2PDF
import pyautogui as loader
from variable_storage import *


class SizeExceeded(Urls, Methods, DocX2PDF):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Docx2Pdf.environment_definition(environment))
        action.wait(DocX2PDF.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(DocX2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Office formats\WORD\3.doc")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(DocX2PDF.msg)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(DocX2PDF.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(DocX2PDF.msg)
        sleep(3)

        try:
            assert "free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        action.clicker(DocX2PDF.select)
        sleep(20)
        frame = driver.find_element_by_xpath("//div[contains(@class,'Inner-Container')]/ancestor::div[contains(@class, 'Frame-Middle')]")
        #frame = driver.find_element_by_xpath("//div[contains(@class,'Form-Wrap')]")
        driver.switch_to_frame(frame)
        #action.switch_to_frame("//div[contains(@class,'Page-Container Page-Step3')]")

        action.typewrite('DBC1', DocX2PDF.first_name)
        action.typewrite('DBC1', DocX2PDF.last_name)
        action.typewrite('email@frt.jk', DocX2PDF.e_mail)
        action.typewrite('DBC1', DocX2PDF.last_name)
        action.typewrite(zip_code, DocX2PDF.zip)
        action.typewrite('DBC1 DBC1', DocX2PDF.card_holder_name)
        action.typewrite('4000000000000002', DocX2PDF.card_number)
        action.typewrite('03', DocX2PDF.month)
        action.typewrite("2021", DocX2PDF.year)
        action.typewrite('256', DocX2PDF.security_code)

        action.clicker(DocX2PDF.process_order)
        action.wait(DocX2PDF.final)
        sleep(1)
        driver.quit()


class DailyLimit(Urls, Methods, DocX2PDF):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Docx2Pdf.environment_definition(environment))
        action.wait(DocX2PDF.anchor)
        loader.press('f12')
        sleep(2)
        network()

        action.clicker(DocX2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Office formats\WORD\small.docx")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(DocX2PDF.hide)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(DocX2PDF.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        sleep(2)
        action.clicker(DocX2PDF.hide)
        sleep(2)
        action.clicker(DocX2PDF.download_btn)
        sleep(7)
        print(f"File is downloaded {next(generator)} time")
        action.go_back()

        action.wait(DocX2PDF.anchor)
        sleep(1)
        action.clicker(DocX2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Office formats\WORD\small.docx")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(DocX2PDF.hide)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(DocX2PDF.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        sleep(2)
        action.clicker(DocX2PDF.hide)
        sleep(2)
        action.clicker(DocX2PDF.download_btn)
        sleep(7)
        print(f"File is downloaded {next(generator)} times")
        action.go_back()

        action.wait(DocX2PDF.anchor)
        sleep(1)
        action.clicker(DocX2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Office formats\WORD\small.docx")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(DocX2PDF.hide)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(DocX2PDF.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        sleep(2)
        action.clicker(DocX2PDF.hide)
        sleep(2)
        action.clicker(DocX2PDF.download_btn)
        sleep(7)
        print(f"File is downloaded {next(generator)} times")
        action.go_back()

        action.wait(DocX2PDF.anchor)
        sleep(1)
        action.clicker(DocX2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Office formats\WORD\small.docx")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(DocX2PDF.msg)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(DocX2PDF.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(DocX2PDF.msg)
        sleep(3)

        try:
            assert "Oops! Looks like you've really" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily limit logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    # init = DailyLimit
    # driver = chrome_setup()
    # init.four_files_loader(driver)

    init = SizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
