from Initialization import chrome_setup  # , firefox_setup, edge_setup, chrome_incognito
from Urls import Urls
from Methods import Methods
from Locators import Converter
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class ConverterSizeExceeded(Urls, Methods, Converter):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Converter.environment_definition(environment))
        action.wait(Converter.anchor)
        action.clicker(Converter.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\MINI.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(Converter.word)
        sleep(1)
        action.clicker(Converter.convert_file)
        sleep(1)
        action.wait(Converter.to_wait)
        sleep(2)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class ConverterDailyLimit(Urls, Methods, Converter):

    @staticmethod
    def four_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Converter.environment_definition(environment))
        action.wait(Converter.anchor)
        action.clicker(Converter.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(Converter.word)
        sleep(1)
        action.clicker(Converter.convert_file)
        sleep(1)
        action.wait(Converter.download)
        sleep(3)
        action.clicker(Converter.download)
        sleep(5)
        action.go_back()

        action.wait(Converter.anchor)
        action.clicker(Converter.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(Converter.excel)
        sleep(1)
        action.clicker(Converter.convert_file)
        sleep(1)
        action.wait(Converter.download)
        sleep(3)
        action.clicker(Converter.download)
        sleep(5)
        action.go_back()

        action.wait(Converter.anchor)
        action.clicker(Converter.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(Converter.ppt)
        sleep(1)
        action.clicker(Converter.convert_file)
        sleep(1)
        action.wait(Converter.download)
        sleep(3)
        action.clicker(Converter.download)
        sleep(5)
        action.go_back()

        action.wait(Converter.anchor)
        action.clicker(Converter.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(Converter.word)
        sleep(1)
        action.clicker(Converter.convert_file)
        sleep(1)
        action.wait(Converter.to_wait)
        sleep(2)

        try:
            assert "enjoyed using our free service" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    # init = ConverterDailyLimit
    # driver = chrome_setup()
    # init.four_file_loader(driver)

    init = ConverterSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
