from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import Protect
import pyautogui as loader
from variable_storage import *
import sys

location = location_definer()
password = password()


class SizeExceeded(Urls, Methods, Protect):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.ProtectPdf.environment_definition(environment))
        action.wait(Protect.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(Protect.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\MINI.pdf")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Protect.protect_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Protect.protect_btn)
        sleep(1)
        action.typewrite(password, Protect.password_field)
        sleep(1)
        action.typewrite(password, Protect.confirm_password_field)
        sleep(1)
        action.clicker(Protect.protect_btn)

        while True:
            try:
                success = action.check_exists_by_xpath(Protect.ok_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        action.wait(Protect.password_field_within_editor)
        sleep(1)
        action.typewrite(password, Protect.password_field_within_editor)
        sleep(1)
        action.clicker(Protect.ok_btn)
        action.wait(Protect.waitable)

        try:
            assert "free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"Screenshot of {location} Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class DailyLimit(Urls, Methods, Protect):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.ProtectPdf.environment_definition(environment))
        action.wait(Protect.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(Protect.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Payslip.pdf")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Protect.protect_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Protect.protect_btn)
        sleep(1)
        action.typewrite(password, Protect.password_field)
        sleep(1)
        action.typewrite(password, Protect.confirm_password_field)
        sleep(0.5)
        action.clicker(Protect.protect_btn)

        while True:
            try:
                success = action.check_exists_by_xpath(Protect.ok_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.typewrite(password, Protect.password_field_within_editor)
        sleep(1)
        action.clicker(Protect.ok_btn)
        action.wait(Protect.preview)
        sleep(1)
        action.clicker(Protect.preview)
        sleep(2)
        action.clicker(Protect.download_btn)
        sleep(5)
        print(f"File has been downloaded {next(generator)} time")
        action.go_back()
        action.go_back()
        # 2 download cycle
        action.wait(Protect.anchor)
        sleep(2)
        action.clicker(Protect.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Payslip.pdf")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Protect.protect_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Protect.protect_btn)
        sleep(1)
        action.typewrite(password, Protect.password_field)
        sleep(1)
        action.typewrite(password, Protect.confirm_password_field)
        sleep(0.5)
        action.clicker(Protect.protect_btn)

        while True:
            try:
                success = action.check_exists_by_xpath(Protect.ok_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.typewrite(password, Protect.password_field_within_editor)
        sleep(1)
        action.clicker(Protect.ok_btn)
        action.wait(Protect.preview)
        sleep(1)
        action.clicker(Protect.preview)
        sleep(2)
        action.clicker(Protect.download_btn)
        sleep(5)
        print(f"File has been downloaded {next(generator)} times")
        action.go_back()
        action.go_back()
        # 3 download cycle
        action.wait(Protect.anchor)
        sleep(2)
        action.clicker(Protect.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Payslip.pdf")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Protect.protect_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Protect.protect_btn)
        sleep(1)
        action.typewrite(password, Protect.password_field)
        sleep(1)
        action.typewrite(password, Protect.confirm_password_field)
        sleep(0.5)
        action.clicker(Protect.protect_btn)

        while True:
            try:
                success = action.check_exists_by_xpath(Protect.ok_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.typewrite(password, Protect.password_field_within_editor)
        sleep(1)
        action.clicker(Protect.ok_btn)
        action.wait(Protect.preview)
        sleep(1)
        action.clicker(Protect.preview)
        sleep(2)
        action.clicker(Protect.download_btn)
        sleep(5)
        print(f"File has been downloaded {next(generator)} times")
        action.go_back()
        action.go_back()
        # 4 download cycle
        action.wait(Protect.anchor)
        sleep(2)
        action.clicker(Protect.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Payslip.pdf")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(Protect.protect_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(Protect.protect_btn)
        sleep(1)
        action.typewrite(password, Protect.password_field)
        sleep(1)
        action.typewrite(password, Protect.confirm_password_field)
        sleep(1)
        action.clicker(Protect.protect_btn)

        while True:
            try:
                success = action.check_exists_by_xpath(Protect.ok_btn)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(Protect.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass
        action.wait(Protect.password_field_within_editor)
        sleep(1)
        action.typewrite(password, Protect.password_field_within_editor)
        sleep(1)
        action.clicker(Protect.ok_btn)
        action.wait(Protect.waitable)

        try:
            assert "Oops! Looks like you've really" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"Screenshot of {location} Daily Limit logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = DailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = SizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
