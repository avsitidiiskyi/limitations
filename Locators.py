from selenium.webdriver.common.by import By


class PdfMerge:
    anchor = (By.XPATH, "//*[contains(@class, 'svg-logo')]")
    choose_file = (By.XPATH, "//input[@name = 'files']//following::label[1]")
    merge_file_btn = (By.XPATH, "//a[contains(text(),'Merge Files')]")

    hide = (By.XPATH, "//span[contains(text(),'Preview File')]")
    download_btn = (By.XPATH, "//span[@class='text ng-scope']")
    anchor_2 = (By.XPATH, "//*[contains(@class, 'paywall-box__continue')]")
    anchor_3 = (By.XPATH, "//p[contains(text(),'Merging Result.pdf')]")


class Converter(PdfMerge):
    convert_file = (By.XPATH, "//span[contains(text(),'Convert file')]")
    excel = (By.XPATH, "//span[@class='icon ic-excel']")
    word = (By.XPATH, "//span[@class='icon ic-word']")
    ppt = (By.XPATH, "//span[@class='icon ic-power-point']")
    image = (By.XPATH, "//span[@class='svg-el']")

    download = (By.XPATH, "//a[contains(text(),'Download file')]")
    to_wait = (By.XPATH, "//p[contains(text(),'To continue, select an option:')]")
    # "//h2[contains(text(),'Get unlimited access to our PDF Tools')]")


class Compressor(Converter):
    low_quality = (By.XPATH, "//span[contains(text(),'Low Quality')]")
    moderate_quality = (By.XPATH, "//p[contains(text(),'favorable quality, moderate level of compression')]")
    best_quality = (By.XPATH, "//p[contains(text(),'best quality of image, minimal compression done')]")
    compress_files = (By.XPATH, "//span[contains(text(),'Compress PDF Files')]")


class GifToPDF(Compressor):
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    text = (By.XPATH, '//div[contains(text(),"Oops! Looks like you")]')


class PDF2HTML(GifToPDF):
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    dwnlwd = (By.XPATH, "//a[contains(text(),'Download file')]")
    get = (By.XPATH, "//h2[contains(text(),'Get unlimited access to our PDF Tools')]")

    to_wait = (By.XPATH, "//p[contains(text(),'To continue, select an option:')]")


class PDF2PPT(Converter):
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    dwnlwd = (By.XPATH, "//a[contains(text(),'Download file')]")
    get = (By.XPATH, "//h2[contains(text(),'Get unlimited access to our PDF Tools')]")


class JPG2PDF(Converter):
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    pass


class Gif2Png(Converter):
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    to_wait = (By.XPATH, "//p[contains(text(),'To continue, select an option:')]")
    dwnlwd = (By.XPATH, "//a[contains(text(),'Download file')]")
    fail = (By.XPATH, "//*[contains(@class, 'error-message error-visible')]")


class Jpg2Png(Converter):
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    to_wait = (By.XPATH, "//p[contains(text(),'To continue, select an option:')]")
    dwnlwd = (By.XPATH, "//a[contains(text(),'Download file')]")


class WaterMark(Gif2Png, PdfMerge):
    file = (By.XPATH, "//span[@class='radio']//following::span[1][contains(text(),'File')]")
    text = (By.XPATH, "//span[@class='radio']//following::span[1][contains(text(),'Text')]")
    above_page = (By.XPATH, "//span[@class='radio']//following::span[1][contains(text(),'Above Page')]")
    text_field = (By.XPATH, '//div[@class="source-type-text"]//child::input[@type="text"]')

    add_watermark = (By.XPATH, "//span[contains(text(),'Add Watermark')]")
    waitable = (By.XPATH, "//span[contains(text(),'To continue, select an option:')]")

class Checkout:
    first_name = (By.XPATH, "//input[@placeholder = 'First Name']")
    last_name = (By.XPATH, "//input[@placeholder = 'Last Name']")
    e_mail = (By.XPATH, "//input[@placeholder = 'Email']")
    zip = (By.XPATH, "//input[@placeholder = 'Zip / Postal Code']")
    card_holder_name = (By.XPATH, "//input[@autocomplete = 'billing cc-name']")
    card_number = (By.XPATH, "//input[@placeholder = 'Card Number']")
    #exp. date
    month = (By.XPATH, "//input[@list = 'monthlist']")
    year = (By.XPATH, "//input[@list = 'expyearlist']")
    security_code = (By.XPATH, "//input[@placeholder = 'Security Code']")
    process_order = (By.XPATH, "//input[@type = 'submit']")
    select = (By.XPATH, "//*[@class='paywall-bf-product-continue']")
    final = (By.XPATH, "//button[contains(text(),'Create Password')]")


class DocX2PDF(Gif2Png, PdfMerge, Checkout):
    msg = (By.XPATH, "//span[contains(text(),'To continue, select an option:')]")
    switchable = (By.XPATH, "//div[contains(@class,'Form-Wrap')]")


class Protect:
    choose_file = (By.XPATH, "//input[@id = 'choose-file']//following::label[1]")
    password_field = (By.XPATH, "//input[@id='password']")
    confirm_password_field = (By.XPATH, "//input[@id='confirm-password']")
    protect_btn = (By.XPATH, "//span[contains(text(),'Protect')]")
    password_field_within_editor = (By.XPATH, "//body/div[@id='fancybox-container-1']/div[2]/div[4]/div[1]/div[1]/"
                                              "section[1]/div[1]/div[2]/input[1]")
    ok_btn = (By.XPATH, "//a[contains(text(),'Ok')]")
    preview = (By.XPATH, "//span[contains(text(),'Preview File')]")
    download_btn = (By.XPATH, "//span[@class='text ng-scope']")
    anchor = (By.XPATH, "//*[contains(@class, 'svg-logo')]")
    fail = (By.XPATH, "//*[contains(@class, 'error-message error-visible')]")
    waitable = (By.XPATH, "//span[contains(text(),'To continue, select an option:')]")


class Checkout:
    first_name = (By.XPATH, "//input[@placeholder = 'First Name']")
    last_name = (By.XPATH, "//input[@placeholder = 'Last Name']")
    e_mail = (By.XPATH, "//input[@placeholder = 'Email']")
    zip = (By.XPATH, "//input[@placeholder = 'Zip / Postal Code']")
    card_holder_name = (By.XPATH, "//input[@autocomplete = 'billing cc-name']")
    card_number = (By.XPATH, "//input[@placeholder = 'Card Number']")
    #exp. date
    month = (By.XPATH, "//input[@list = 'monthlist']")
    year = (By.XPATH, "//input[@list = 'expyearlist']")
    security_code = (By.XPATH, "//input[@placeholder = 'Security Code']")
    process_order = (By.XPATH, "//input[@type = 'submit']")
    select = (By.XPATH, "//*[@class='paywall-bf-product-continue']")
    final = (By.XPATH, "//button[contains(text(),'Create Password')]")
    switchable = (By.XPATH, "//div[contains(@class,'Page-Container Page-Step3')]")

    var1 = '//div[@id="checkout-panel"]'
    var2 = "//div[contains(@class,'sitemenu-tab-content convert paywall-wrapper')]"
    var3 = "//div[contains(@class,'Inner-Container')]/ancestor::div[contains(@class, 'Frame-Middle')]"





# //div[contains(@class,"Page-Container Page-Step3"')]//following::input[@type='text'] = range btn //*[@class='Frame-Middle']

# //div[contains(@class, 'field')]//following::watermark[1]/div[1]/div[1]/div[3]/div[1]/input[1]

# //*[text()[contains(.,'text')]]   //div[@class="source-type-text"]//*[text="Example"]
