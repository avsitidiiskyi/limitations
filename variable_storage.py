import ast
import requests
import pyautogui
from time import sleep
from string import ascii_letters
import random
from random import choice


RED = '\033[0;31m'
GREEN = '\033[0;36m'
WHITE = '\033[0;37m'

while True:
    environment = input("What environment do you need?: ")
    if environment == 'www' or environment == 'stage':
        break
    else:
        print("Environment, please....")


def location_definer():
    response = requests.get('https://api.2ip.ua/geo.json?ip=')
    result = ast.literal_eval(response.text)
    return result['country']


location = location_definer()


def network():
    sleep(1)
    pyautogui.moveTo(1666, 128)
    sleep(2)
    pyautogui.click()
    sleep(1)
    pyautogui.moveTo(1510, 155)
    sleep(1)
    pyautogui.click()


def log_loader():
    sleep(1)
    pyautogui.moveTo(1836, 158)
    sleep(1)
    pyautogui.click()
    sleep(3)
    pyautogui.press('enter')
    sleep(8)


def password():
    return (''.join(choice(ascii_letters) for word
            in range(7))) + str(random.randrange(900))


def counter():
    for num in range(1, 6):
        yield num


generator = counter()

postal_codes = {

    "United States": "12901",
    "Canada": "H0H 0H0",
    "Australia": "2000",
    "Austria": "1010",
    "Denmark": "75116",
    "Germany": "75116",
    "Ireland": "75116",
    "Sweden": "75116",
    "United Kingdom": "WC2R 1HA",
    "Switzerland": "9485"
}


zip_code = [postal_codes[x] for x in postal_codes if x == location][0]



