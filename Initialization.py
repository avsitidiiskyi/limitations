from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium import webdriver
from Path import Path
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options


def chrome_setup():
    driver = webdriver.Chrome(Path.chromedriver)
    driver.maximize_window()
    return driver


def chrome_headless():
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome(executable_path=Path.chromedriver, options=options)
    driver.set_window_size(1920, 1080)
    return driver


def chrome_incognito():
    chrome_options = Options()
    chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=Path.chromedriver)
    driver.maximize_window()
    return driver


def firefox_setup():
    driver = webdriver.Firefox(executable_path=Path.firefoxdriver)
    driver.maximize_window()
    return driver


def edge_setup():
    driver = webdriver.Edge(executable_path=Path.edgedriver)
    driver.maximize_window()
    return driver


class Settings:

    def __init__(self, driver):
        self.driver = driver

    def find_element(self, locator, time=4000):
        return WebDriverWait(self.driver, time).until(ec.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=999):
        return WebDriverWait(self.driver, time).until(ec.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def is_clickable(self, locator, time=4000):
        return WebDriverWait(self.driver, time).until(ec.element_to_be_clickable(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def open_url(self, url):
        return self.driver.get(url)

    def open_new_url(self, new_url):
        return self.driver.execute_script(f"window.open('{new_url}', 'new_window')")

    def switch_to_window(self, index):
        return self.driver.switch_to_window(self.driver.window_handles[index])

    def scroll_by_pixels(self, value):
        return self.driver.execute_script(f"window.scrollBy(0,{value})", "")

    def scroll_to_end(self):
        return self.driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")

    def scroll_until_locate(self, elem):
        return self.driver.execute_script("arguments[0].scrollIntoView();", elem)

    def refresh(self):
        return self.driver.refresh()

    def move_to(self, locator):
        element = self.find_element(locator)
        action = ActionChains(self.driver)
        return action.move_to_element(element).perform()

    def switch_to_frame(self, frame_name):
        return self.driver.switch_to.frame(self.driver.find_element_by_xpath(frame_name))

    def go_back(self):
        return self.driver.back()

    def go_forward(self):
        return self.driver.forward()

