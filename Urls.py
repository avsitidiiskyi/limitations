class Urls:

    class PdfMerge:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/merge-pdf/"

    class Converter:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/pdf-converter/"

    class Compressor:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/compress-pdf/"

    class Gif2Pdf:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/jpg-to-pdf/"

    class Pdf2Html:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/pdf-to-html"

    class Pdf2ppt:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/pdf-to-ppt/"

    class J2P:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/jpg-to-pdf/"

    class GIF2PNG:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/gif-to-png/"

    class JPG2PGN:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/jpg-to-png/"

    class Watermark:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/add-watermark-to-pdf/"

    class Docx2Pdf:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/docx-to-pdf/"

    class ProtectPdf:
        @staticmethod
        def environment_definition(environment):
            return f"https://{environment}.sodapdf.com/password-protect-pdf/"


