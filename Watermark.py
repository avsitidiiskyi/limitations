from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import WaterMark
import pyautogui as loader
from variable_storage import *
location = location_definer()


class SizeExceeded(Urls, Methods, WaterMark):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Watermark.environment_definition(environment))
        action.wait(WaterMark.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(WaterMark.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\MINI.pdf")
        loader.press('enter')
        sleep(5)
        while True:
            try:
                success = action.check_exists_by_xpath(WaterMark.add_watermark)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(WaterMark.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(WaterMark.add_watermark)
        sleep(1)
        action.typewrite('SizeExceeded', WaterMark.text_field)
        sleep(1)
        action.clicker(WaterMark.above_page)
        action.spotter(WaterMark.add_watermark)
        sleep(2)
        loader.click(672, 693)
        loader.scroll(350)
        action.clicker(WaterMark.add_watermark)
        action.wait(WaterMark.waitable)
        sleep(3)

        try:
            assert "free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class DailyLimit(Urls, Methods, WaterMark):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Watermark.environment_definition(environment))

        action.wait(WaterMark.anchor)
        loader.press('f12')
        sleep(2)
        network()
        action.clicker(WaterMark.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(WaterMark.add_watermark)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(WaterMark.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(WaterMark.add_watermark)
        sleep(1)
        action.typewrite('Daily Limit', WaterMark.text_field)
        sleep(1)
        action.clicker(WaterMark.above_page)
        action.spotter(WaterMark.add_watermark)
        sleep(2)

        loader.click(672, 693)
        loader.scroll(350)
        action.clicker(WaterMark.add_watermark)

        action.wait(WaterMark.hide)
        sleep(3)
        action.clicker(WaterMark.hide)
        sleep(2)
        action.clicker(WaterMark.download_btn)
        sleep(3)
        action.go_back()

        action.wait(WaterMark.anchor)
        sleep(2)
        action.clicker(WaterMark.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(WaterMark.add_watermark)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(WaterMark.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(WaterMark.add_watermark)
        sleep(1)
        action.typewrite('Daily Limit', WaterMark.text_field)
        sleep(1)
        action.clicker(WaterMark.above_page)
        action.spotter(WaterMark.add_watermark)
        sleep(2)
        loader.click(672, 693)
        loader.scroll(350)
        action.clicker(WaterMark.add_watermark)

        action.wait(WaterMark.hide)
        sleep(3)
        action.clicker(WaterMark.hide)
        sleep(2)
        action.clicker(WaterMark.download_btn)
        sleep(3)
        action.go_back()

        action.wait(WaterMark.anchor)
        sleep(2)
        action.clicker(WaterMark.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(WaterMark.add_watermark)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(WaterMark.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(WaterMark.add_watermark)
        sleep(1)
        action.typewrite('Daily Limit', WaterMark.text_field)
        sleep(1)
        action.clicker(WaterMark.above_page)
        action.spotter(WaterMark.add_watermark)
        sleep(2)
        loader.click(672, 693)
        loader.scroll(350)
        action.clicker(WaterMark.add_watermark)

        action.wait(WaterMark.hide)
        sleep(3)
        action.clicker(WaterMark.hide)
        sleep(2)
        action.clicker(WaterMark.download_btn)
        sleep(3)
        action.go_back()

        action.wait(WaterMark.anchor)
        sleep(2)
        action.clicker(WaterMark.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Small 2.pdf")
        loader.press('enter')

        while True:
            try:
                success = action.check_exists_by_xpath(WaterMark.add_watermark)
                if success:
                    break
            except Exception:
                pass
            try:
                error = action.check_exists_by_xpath(WaterMark.fail)
                if error:
                    log_loader()

                    driver.quit()
                    break
            except Exception:
                pass

        action.wait(WaterMark.add_watermark)
        sleep(1)
        action.typewrite('Daily limit', WaterMark.text_field)
        sleep(1)
        action.clicker(WaterMark.above_page)
        action.spotter(WaterMark.add_watermark)
        sleep(2)
        loader.click(672, 693)
        loader.scroll(350)
        action.clicker(WaterMark.add_watermark)
        action.wait(WaterMark.waitable)

        try:
            assert "Oops! Looks like you've really" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily limit logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = DailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = SizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
