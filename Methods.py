from Initialization import Settings
from selenium.common.exceptions import NoSuchElementException


class Methods(Settings):

    def clicker(self, locator):
        action = self.find_element(locator, time=10).click()
        return action

    def typewrite(self, word, locator):
        search_field = self.find_element(locator)
        search_field.send_keys(word)
        return search_field

    def upload(self, address, locator):
        element = self.find_element(locator)
        element.send_keys(address)
        return element

    def get_current_url(self):
        return self.driver.current_url

    def get_text_element(self, locator):
        action = self.find_element(locator, time=2)
        return action

    def clear_field(self, locator):
        action = self.find_element(locator, time=2)
        action.clear()
        return action

    def check_exists_by_xpath(self, locator):
        try:
            self.find_element(locator, time=2)
        except NoSuchElementException:
            return False
        return True

    def wait(self, locator):
        action = self.is_clickable(locator, time=4000)
        return action

    def spotter(self, locator):
        element = self.find_element(locator)
        action = self.scroll_until_locate(element)
        return action

    def screenshot(self, location, limitation_type):
        return self.driver.save_screenshot(fr"D:\screenshots\{location} location({limitation_type}).png")
