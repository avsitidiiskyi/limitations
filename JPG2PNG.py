from Initialization import chrome_setup
from Urls import Urls
from Methods import Methods
from Locators import Jpg2Png
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class SizeExceeded(Urls, Methods, Jpg2Png):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.JPG2PGN.environment_definition(environment))
        action.wait(Jpg2Png.anchor)
        action.clicker(Jpg2Png.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Images\less than 5.jpg")
        loader.press('enter')
        sleep(5)

        action.wait(Jpg2Png.to_wait)
        sleep(1)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class DailyLimit(Urls, Methods, Jpg2Png):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.JPG2PGN.environment_definition(environment))

        action.wait(Jpg2Png.anchor)
        action.clicker(Jpg2Png.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        sleep(1)
        loader.press('enter')
        action.wait(Jpg2Png.dwnlwd)
        sleep(3)
        action.clicker(Jpg2Png.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(Jpg2Png.anchor)
        action.clicker(Jpg2Png.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        sleep(1)
        loader.press('enter')
        action.wait(Jpg2Png.dwnlwd)
        sleep(3)
        action.clicker(Jpg2Png.dwnlwd)
        sleep(3)
        action.go_back()

        action.wait(Jpg2Png.anchor)
        action.clicker(Jpg2Png.choose_file)
        sleep(2)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        sleep(1)
        loader.press('enter')
        action.wait(Jpg2Png.dwnlwd)
        sleep(3)
        action.clicker(Jpg2Png.dwnlwd)
        sleep(3)
        action.go_back()

        action.clicker(Jpg2Png.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Images\QA.jpg")
        loader.press('enter')
        sleep(5)

        action.wait(Jpg2Png.to_wait)
        sleep(1)

        try:
            assert "Oops! Looks like you've really" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = DailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = SizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
