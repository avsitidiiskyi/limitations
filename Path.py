import os


class Path:
    chromedriver = os.path.join(os.path.dirname(__file__), "C:\Webdrivers\chromedriver.exe")
    firefoxdriver = os.path.join(os.path.dirname(__file__), r'C:\Webdrivers\geckodriver.exe')
    edgedriver = os.path.join(os.path.dirname(__file__), "C:\Webdrivers\msedgedriver.exe")
