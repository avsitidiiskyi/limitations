from Initialization import chrome_setup  # , firefox_setup, edge_setup, chrome_incognito
from Urls import Urls
from Methods import Methods
from Locators import PdfMerge
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class MergeSizeExceeded(Urls, Methods, PdfMerge):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.PdfMerge.environment_definition(environment))
        action.wait(PdfMerge.anchor)
        action.clicker(PdfMerge.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs")
        loader.press('enter')
        sleep(1)
        loader.typewrite('"~7.5.pdf" "small 1.pdf"')
        sleep(1)
        loader.press('enter')
        sleep(5)
        action.clicker(PdfMerge.merge_file_btn)
        action.wait(PdfMerge.anchor_2)
        sleep(1)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class MergeDailyLimit(Urls, Methods, PdfMerge):

    @staticmethod
    def four_files_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.PdfMerge.environment_definition(environment))

        action.wait(PdfMerge.anchor)
        action.clicker(PdfMerge.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs")
        sleep(1)
        loader.press('enter')
        sleep(1)
        loader.typewrite('"Small 2.pdf" "small 1.pdf" ')
        sleep(1)
        loader.press('enter')
        sleep(5)
        action.clicker(PdfMerge.merge_file_btn)
        action.wait(PdfMerge.anchor_3)
        sleep(3)
        action.clicker(PdfMerge.hide)
        sleep(2)
        action.clicker(PdfMerge.download_btn)
        sleep(7)
        action.go_back()
        action.wait(PdfMerge.anchor)
        sleep(1)

        action.wait(PdfMerge.anchor)
        action.clicker(PdfMerge.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs")
        sleep(1)
        loader.press('enter')
        sleep(1)
        loader.typewrite('"Small 2.pdf" "small 1.pdf" ')
        sleep(1)
        loader.press('enter')
        sleep(5)
        action.clicker(PdfMerge.merge_file_btn)
        action.wait(PdfMerge.anchor_3)
        sleep(3)
        action.clicker(PdfMerge.hide)
        sleep(2)
        action.clicker(PdfMerge.download_btn)
        sleep(7)
        action.go_back()
        action.wait(PdfMerge.anchor)
        sleep(1)

        action.wait(PdfMerge.anchor)
        action.clicker(PdfMerge.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs")
        sleep(1)
        loader.press('enter')
        sleep(1)
        loader.typewrite('"Small 2.pdf" "small 1.pdf" ')
        sleep(1)
        loader.press('enter')
        sleep(5)
        action.clicker(PdfMerge.merge_file_btn)
        action.wait(PdfMerge.anchor_3)
        sleep(3)
        action.clicker(PdfMerge.hide)
        sleep(2)
        action.clicker(PdfMerge.download_btn)
        sleep(7)
        action.go_back()
        action.wait(PdfMerge.anchor)
        sleep(1)

        action.clicker(PdfMerge.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs")
        loader.press('enter')
        sleep(1)
        loader.typewrite('"Small 2.pdf" "small 1.pdf" ')
        loader.press('enter')
        sleep(5)
        action.clicker(PdfMerge.merge_file_btn)
        action.wait(PdfMerge.anchor_2)
        sleep(3)

        try:
            assert "You've reached your limit of 3 tasks per hour" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    init = MergeDailyLimit
    driver = chrome_setup()
    init.four_files_loader(driver)

    init = MergeSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
