from Initialization import chrome_setup  # , firefox_setup, edge_setup, chrome_incognito
from Urls import Urls
from Methods import Methods
from Locators import JPG2PDF
from time import sleep
import pyautogui as loader
from variable_storage import *
location = location_definer()


class JPG2PDFSizeExceeded(Urls, Methods, JPG2PDF):

    @staticmethod
    def one_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.J2P.environment_definition(environment))
        action.wait(JPG2PDF.anchor)
        action.clicker(JPG2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\Images\less than 5.jpg")
        sleep(1)
        loader.press('enter')
        action.wait(JPG2PDF.to_wait)
        sleep(2)

        try:
            assert "You've reached your free file size limit" in driver.page_source
            action.screenshot(location, 'SizeExceeded')
            print(f"screenshot of Size Exceeded logic is taken")
        except AssertionError:
            print("'Size limit text' is not located")
        driver.quit()


class ConverterDailyLimit(Urls, Methods, JPG2PDF):

    @staticmethod
    def four_file_loader(browser):
        action = Methods(browser)
        action.open_url(Urls.Converter.environment_definition(environment))
        action.wait(JPG2PDF.anchor)
        action.clicker(JPG2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(JPG2PDF.word)
        sleep(1)
        action.clicker(JPG2PDF.convert_file)
        sleep(1)
        action.wait(JPG2PDF.download)
        sleep(3)
        action.clicker(JPG2PDF.download)
        sleep(5)
        action.go_back()

        action.wait(JPG2PDF.anchor)
        action.clicker(JPG2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(JPG2PDF.excel)
        sleep(1)
        action.clicker(JPG2PDF.convert_file)
        sleep(1)
        action.wait(JPG2PDF.download)
        sleep(3)
        action.clicker(JPG2PDF.download)
        sleep(5)
        action.go_back()

        action.wait(JPG2PDF.anchor)
        action.clicker(JPG2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(JPG2PDF.ppt)
        sleep(1)
        action.clicker(JPG2PDF.convert_file)
        sleep(1)
        action.wait(JPG2PDF.download)
        sleep(3)
        action.clicker(JPG2PDF.download)
        sleep(5)
        action.go_back()

        action.wait(JPG2PDF.anchor)
        action.clicker(JPG2PDF.choose_file)
        sleep(3)
        loader.typewrite(r"C:\Test data\PDFs\Text_In_PDF.pdf")
        sleep(1)
        loader.press('enter')
        sleep(3)
        action.clicker(JPG2PDF.word)
        sleep(1)
        action.clicker(JPG2PDF.convert_file)
        sleep(1)
        action.wait(JPG2PDF.to_wait)
        sleep(2)

        try:
            assert "enjoyed using our free service" in driver.page_source
            action.screenshot(location, 'DailyLimit')
            print(f"screenshot of Daily logic is taken")
        except AssertionError:
            print("'Daily limit text' is not located")
        driver.quit()


if __name__ == "__main__":

    # init = ConverterDailyLimit
    # driver = chrome_setup()
    # init.four_file_loader(driver)

    init = JPG2PDFSizeExceeded
    driver = chrome_setup()
    init.one_file_loader(driver)
